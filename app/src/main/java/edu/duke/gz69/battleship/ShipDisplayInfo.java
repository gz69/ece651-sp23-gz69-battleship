package edu.duke.gz69.battleship;

public interface ShipDisplayInfo<T> {
    T getInfo(Coordinate where, boolean hit);
}

