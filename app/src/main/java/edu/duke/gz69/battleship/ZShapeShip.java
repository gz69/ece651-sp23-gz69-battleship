package edu.duke.gz69.battleship;

import java.util.HashMap;
import java.util.HashSet;

import static edu.duke.gz69.battleship.TShapeShip.generateCoords;

/**
 * Carriers' new style
 * @param <T>
 */
public class ZShapeShip<T> extends BasicShip<T> {
    private final String name;

    /**
     * Make all the set of coordinates for a battleship starting
     * at upperLeft like
     * <p>
     *          Carriers:
     *                   C                       C
     *                   c          *cccc        cc       * ccc
     *                   cc   OR    ccc      OR  cc   OR  cccc
     *                   cc                       c
     *                    c                       c
     * <p>
     *                  Up           Right     Down          Left
     * @param place the start point of the ship
     * @return the set of Coordinates, e.g. set {(row=1,col=2), (row=2,col=2), (row=3,col=2)}
     */
    static HashSet<Coordinate> makeCoords(Placement place) {
        Character orientation = place.getOrientation();
        if (!"UDLRudlr".contains(orientation.toString())) {
            throw new IllegalArgumentException("Invalid orientation: " + orientation);
        }
        int row = place.getWhere().getRow();
        int col = place.getWhere().getColumn();
        int[][] offsets = null;
        if (orientation == 'U' || orientation == 'u') {
            offsets = new int[][]{{0, 0}, {1, 0}, {2, 0}, {3, 0}, {2, 1}, {1, 1}};
        } else if (orientation == 'D' || orientation == 'd') {
            offsets = new int[][]{{0, 0}, {1, 0}, {2, 0}, {3, 0}, {1, 1}, {2, 1}};
        } else if (orientation == 'L' || orientation == 'l') {
            offsets = new int[][]{{0, 0}, {0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 1}};
        } else if (orientation == 'R' || orientation == 'r') {
            offsets = new int[][]{{0, 0}, {0, 1}, {0, 2}, {0, 3}, {1, 1}, {1, 2}};
        }
        HashSet<Coordinate> coords = new HashSet<>();
        for (int[] offset : offsets) {
            coords.add(new Coordinate(row + offset[0], col + offset[1]));
        }
        return coords;
    }


    static HashMap<Coordinate, Integer> makeMaps(Placement place) {
//        Coordinate upperLeft = place.getWhere();
//        char orientation = place.getOrientation();
//        if(orientation.equals('V') || orientation.equals('H')) {
//            throw new IllegalArgumentException(
//                    "the orientation of this type of ship must be" +
//                            "'U' or 'u' or 'D' or 'd' or 'L' or 'l' or 'R' or 'r' but is " + orientation
//            );
//        }
//        int row = upperLeft.getRow();
//        int col = upperLeft.getColumn();
        HashMap<Coordinate, Integer> shipMaps = new HashMap<>();
//        if (orientation == 'U') {
//            shipMaps.put(new Coordinate(row, col + 1), 1);
//            shipMaps.put(new Coordinate(row + 1, col), 2);
//            shipMaps.put(new Coordinate(row + 1, col + 1), 3);
//            shipMaps.put(new Coordinate(row + 1, col + 2), 4);
//        } else if (orientation == 'R') {
//            shipMaps.put(new Coordinate(row + 1, col + 1), 1);
//            shipMaps.put(new Coordinate(row, col), 2);
//            shipMaps.put(new Coordinate(row + 1, col), 3);
//            shipMaps.put(new Coordinate(row + 2, col), 4);
//        } else if (orientation == 'D'){
//            shipMaps.put(new Coordinate(row + 1, col + 1), 1);
//            shipMaps.put(new Coordinate(row, col + 2), 2);
//            shipMaps.put(new Coordinate(row, col + 1), 3);
//            shipMaps.put(new Coordinate(row, col), 4);
//        }else if (orientation == 'L') {
//            shipMaps.put(new Coordinate(row + 1, col), 1);
//            shipMaps.put(new Coordinate(row, col + 1), 4);
//            shipMaps.put(new Coordinate(row + 1, col + 1), 3);
//            shipMaps.put(new Coordinate(row + 2, col + 1), 2);
//        }
        return shipMaps;
    }
    /**
     * Construct a Z shape ship with the Coordinates and the Display info
     *
     * @param place           is an Iterable set of Coordinates this Ship occupies
     * @param selfDisplayInfo    is a member to help check what is the info in the Coordinate
     * @param enemyDisplayInfo is
     * @param name is the name of ship
     */
    public ZShapeShip(String name, Placement place,
                      SimpleShipDisplayInfo<T> selfDisplayInfo, SimpleShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(place), selfDisplayInfo, enemyDisplayInfo, makeMaps(place));
        this.name = name;
    }

    public ZShapeShip(String name, Placement p, T data, T onHit) {
        this(name, p, new SimpleShipDisplayInfo<>(data, onHit), new SimpleShipDisplayInfo<>(null, data));
    }

    @Override
    public String getName() {
        return name;
    }
}

