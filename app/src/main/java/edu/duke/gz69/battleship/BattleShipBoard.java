package edu.duke.gz69.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T> {
    private final int width;
    private final int height;
    private final PlacementRuleChecker<T> placementChecker;
    private final ArrayList<Ship<T>> myShips;
    private final T missInfo;
    HashSet<Coordinate> enemyMisses;
    /**
     * Constructs a BattleShipBoard with the specified width and height
     *
     * @param w       is the width of the newly constructed board
     * @param h       is the height of the newly constructed board
     * @throws IllegalArgumentException if the width or height are less than or equal to 0
     */
    public BattleShipBoard(int w, int h, PlacementRuleChecker<T> placementChecker, T missInfo) {
        this.myShips = new ArrayList<>();
        if(w <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
        }
        if(h <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
        }
        this.width = w;
        this.height = h;
        this.placementChecker = placementChecker;
        this.enemyMisses = new HashSet<>();
        this.missInfo = missInfo;
    }

    public BattleShipBoard(int w, int h, T missInfo) {
        this(w, h, new InBoundsRuleChecker<>(new NoCollisionRuleChecker<>(null)),missInfo);
    }

    public Ship<T> fireAt(Coordinate c) {
        for (Ship<T> ship: myShips) {
            if(ship.occupiesCoordinates(c)) {
                ship.recordHitAt(c);
                return ship;
            }
        }

        enemyMisses.add(c);
        return null;
    }

    public Ship<T> getShipAt(Coordinate c) {
        for (Ship<T> ship: myShips) {
            if(ship.occupiesCoordinates(c)) {
                return ship;
            }
        }
        return null;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /**
     * Add the ship to the list and return true.
     * Check the validity of the placement and returns true if the
     * placement was OK and false otherwise.
     * @param toAdd is the ship to be added
     * @return true if the add action is correctly done, false otherwise
     */
    public String tryAddShip(Ship<T> toAdd) {
        String placementProblem = placementChecker.checkPlacement(toAdd, this);
        if(placementProblem == null) {
            myShips.add(toAdd);
            return null;
        } else {
//            throw new IllegalArgumentException(placementProblem);
            return placementProblem;
        }
    }

    /**
     * Remove the ship from the list and change the hit into the new one.
     *
     * @param shipToRemove is the ship to be removed
     * @return msg if the remove is done, otherwise the message from tryAddShip.
     */
    public String tryRemoveShip(Ship<T> shipToRemove, Ship<T> newShip) {
        ArrayList<Integer> hitIndexes = shipToRemove.getHitIndexes();
        newShip.recordMovedHits(hitIndexes);
        myShips.remove(shipToRemove);

        String msg = tryAddShip(newShip); // if there's a message, there's a problem
        if (msg == null) {
            return "remove successfully!";
        } else {
            return msg;
        }
    }


    /**
     * It takes a Coordinate, and sees which Ship occupies that coordinate.
     * If one is found, we return whatever displayInfo it has at those coordinates.
     * If none, return null.
     * @param where is the specified coordinate to check what it is
     * @return return whatever displayInfo it has at those coordinates
     */
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    /**
     * It takes a Coordinate, and sees which Ship occupies that coordinate.
     * If one is found, we return whatever displayInfo it has at those coordinates.
     * If none, return null.
     * @param where is the specified coordinate to check what it is
     * @return return whatever displayInfo it has at those coordinates
     */
    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    // the moving of ships in version2 will not reveal in what is At
    protected T whatIsAt(Coordinate where, boolean isSelf) {
        if (!isSelf && enemyMisses.contains(where)) {
            return missInfo;
        }

        return myShips.stream()
                .filter(s -> s.occupiesCoordinates(where))
                .findFirst()
                .map(s -> s.getDisplayInfoAt(where, isSelf))
                .orElse(null);
    }


    @Override
    public boolean checkAllSunk() {
        return myShips.stream().allMatch(Ship::isSunk);
    }

    protected boolean checkCoordinateInThisBoard(int row, int col) {
        return row >= 0 && row < height && col >= 0 && col < width;
    }


    protected ArrayList<Coordinate> getSonarArea(Coordinate center, int extend) {
        ArrayList<Coordinate> sonarArea = new ArrayList<>();
        int centerRow = center.getRow();
        int centerCol = center.getColumn();

        for (int row = centerRow - extend; row <= centerRow + extend; row++) {
            for (int col = centerCol - extend; col <= centerCol + extend; col++) {
                if (checkCoordinateInThisBoard(row, col) && Math.abs(centerRow - row) + Math.abs(centerCol - col) <= extend) {
                    Coordinate c = new Coordinate(row, col);
                    sonarArea.add(c);
                }
            }
        }

        return sonarArea;
    }


    public HashMap<String, Integer> sonarFind(Coordinate center) {
        HashMap<String, Integer> detectedNumsOfEachShip = new HashMap<>();
        detectedNumsOfEachShip.put("BattleShip", 0);
        detectedNumsOfEachShip.put("Carrier", 0);
        detectedNumsOfEachShip.put("Destroyer", 0);
        detectedNumsOfEachShip.put("Submarine", 0);

        ArrayList<Coordinate> sonarArea = getSonarArea(center, 3);
        for (Ship<T> ship : myShips) {
            int numDetected = 0;
            for (Coordinate c : sonarArea) {
                if (ship.occupiesCoordinates(c)) {
                    numDetected++;
                }
            }
            if (numDetected > 0) {
                detectedNumsOfEachShip.put(ship.getName(), numDetected);
            }
        }

        return detectedNumsOfEachShip;
    }
}
