package edu.duke.gz69.battleship;

import java.util.HashMap;
import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T>{
    private final String name;

    /**
     * Get the name of ship.
     * @return the name.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Make all the set of coordinates for a rectangle starting
     * at upperLeft whose width and height are as specified
     * @param upperLeft the start point of the ship
     * @param width the width of it
     * @param height the height of it
     * @return the set of Coordinates, e.g. set {(row=1,col=2), (row=2,col=2), (row=3,col=2)}
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Width and height must be positive integers.");
        }

        HashSet<Coordinate> shipCoords = new HashSet<>();
        shipCoords.add(upperLeft);
        int startRow = upperLeft.getRow();
        int startCol = upperLeft.getColumn();

        for (int i = startRow; i < startRow + height; i++) {
            for (int j = startCol; j < startCol + width; j++) {
                shipCoords.add(new Coordinate(i, j));
            }
        }

        return shipCoords;
    }


    static HashMap<Coordinate, Integer> makeMaps(Coordinate upperLeft, int width, int height) {
        HashMap<Coordinate, Integer> shipMaps = new HashMap<>();
//        shipCoords.add(upperLeft);
//        int startRow = upperLeft.getRow();
//        int startCol = upperLeft.getColumn();
//
//        for(int i = startRow; i < startRow + height; i++) {
//            for(int j = startCol; j < startCol + width; j++) {
//                shipCoords.add(new Coordinate(i,j));
//            }
//        }
        return shipMaps;
    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height,
                         SimpleShipDisplayInfo<T> selfDisplayInfo, SimpleShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(upperLeft, width, height), selfDisplayInfo, enemyDisplayInfo, makeMaps(upperLeft,width,height));
        this.name = name;
    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<>(data, onHit),
                new SimpleShipDisplayInfo<>(null, data));
    }

    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("test_ship", upperLeft, 1, 1, data, onHit);
    }
}

