package edu.duke.gz69.battleship;

import java.util.HashMap;

public interface Board<T> {
    int getWidth();
    int getHeight();

    /**
     * Add the ship to the list and return true.
     * Check the validity of the placement and returns true if the
     * placement was OK and false otherwise.
     * @param toAdd is the ship to be added
     * @return true if the add action is correctly done, false otherwise
     */
    String tryAddShip(Ship<T> toAdd);

    /**
     * It takes a Coordinate, and sees which Ship occupies that coordinate.
     * If one is found, we return whatever displayInfo it has at those coordinates.
     * If none, return null.
     * @param where is the specified coordinate to check what it is
     * @return return whatever displayInfo it has at those coordinates
     */
    T whatIsAtForSelf(Coordinate where);
    T whatIsAtForEnemy(Coordinate where);

    /**
     * A way to "shoot" at anything on the board.
     *
     * @param c the coordinate be fired.
     * @return the ship be fired at.
     */
    Ship<T> fireAt(Coordinate c);

    Ship<T> getShipAt(Coordinate c);

    String tryRemoveShip(Ship<T> shipToRemove, Ship<T> newShip);
    /**
     * Test if all the ships are sunk on this board.
     * @return true if the ship on the board are all sunk.
     */
    boolean checkAllSunk();

    HashMap<String, Integer> sonarFind(Coordinate center);
}
