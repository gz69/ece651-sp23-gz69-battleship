package edu.duke.gz69.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

/**
 * The real player class easy contains all the information the game need.
 */
public class TextPlayer {
    final Board<Character> theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
    final AbstractShipFactory<Character> shipFactory1;
    final AbstractShipFactory<Character> shipFactory2;
    final String name;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;

    /*for version_2 params*/
    public int moveRemain = 3;
    public int sonarRemain = 3;

    public TextPlayer(String name, Board<Character> board, BoardTextView view,
                      BufferedReader inputReader, PrintStream out, V1ShipFactory shipFactory1, V2ShipFactory shipFactory2) {
        this.name = name;
        this.theBoard = board;
        this.view = view;
        this.inputReader = inputReader;
        this.out = out;
        this.shipFactory1 = shipFactory1;
        this.shipFactory2 = shipFactory2;
        shipsToPlace = new ArrayList<>();
        setupShipCreationArray();
        shipCreationFns = new HashMap<>();
        setupShipCreationMap();
    }


    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", shipFactory1::makeSubmarine);
        shipCreationFns.put("Destroyer", shipFactory1::makeDestroyer);
        shipCreationFns.put("Battleship", shipFactory2::makeBattleship);
        shipCreationFns.put("Carrier", shipFactory2::makeCarrier);
    }

//    protected void setupShipCreationArray() {
////        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
////        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
//        shipsToPlace.addAll(Collections.nCopies(1, "Battleship"));
////        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
//    }

    protected void setupShipCreationArray() {
        shipsToPlace.addAll(Collections.nCopies(1, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(0, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(0, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(0, "Carrier"));
    }

    /**
     * Print out a prompt and read the placement from the inputReader and return newly constructed placement
     * @param prompt is the prompt message which want to print out
     * @return newly constructed placement
     * @throws IOException when reading errors happen
     */
    public Placement readPlacement(String prompt) throws IOException {
        // Print the prompt to the output stream.
        out.println(prompt);

        // Read the input string from the input reader.
        String s = inputReader.readLine();

        // If the input string is null, throw an exception indicating that an EOF was received unexpectedly.
        if(s == null) {
            throw new EOFException(
                    "Received an EOF when reading placement but " +
                            "there should not be that way."
            );
        }

        // Parse the input string and return a new Placement object.
        return new Placement(s);
    }


    /**
     * Take an input to construct a Placement and place a ship to the board and
     * print out theBoard
     * @throws IOException when encountering some reading and printing errors
     */
    public void doOnePlacement(String shipName) throws IOException {
        String prompt = "Player " + name +
                " where do you want to place a " + shipName + "?";
        boolean placementAccepted = false;
        do {
            try {
                Placement p = readPlacement(prompt);
                Ship<Character> ship = shipCreationFns.get(shipName).apply(p);
                String errMsg = theBoard.tryAddShip(ship);
                if (errMsg == null) {
                    placementAccepted = true;
                } else {
                    out.println(errMsg);
                }
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            }
        } while (!placementAccepted);
        out.println("Current ocean:");
        out.print(view.displayMyOwnBoard());
    }


    /**
     * Print the instruction messages.
     * @return the instruction messages.
     */
    public String printInstructionMsg(){
        return "Player "+name+": you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left\n" +
                "side of the ship, followed by either H (for horizontal) or V (for\n" +
                "vertical).  For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right.  You have\n" +
                "\n" +
                "2 \"Submarines\" ships that are 1x2 \n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are 1x4\n" +
                "2 \"Carriers\" that are 1x6\n";
    }

    /**
     * Do the placement phase for player.
     * @throws IOException when reading placement or printing.
     */
    public void doPlacementPhase() throws IOException {
        for(String shipName: shipsToPlace) {
            out.println("--------------------------------------------------------------------------------");
            out.print(view.displayMyOwnBoard());
            out.println("--------------------------------------------------------------------------------");
            out.println(printInstructionMsg());
            this.doOnePlacement(shipName);
        }
    }

    /**
     * Prompt player to input the coordinate to be fired in the enemy's board.
     * @param prompt the prompt need to be printed out when reading.
     * @return the coordinate to be fired at.
     * @throws IOException when printing or reading.
     */
    public Coordinate readCoordinateAt(String prompt) throws IOException {
        out.println(prompt);
        String coords = inputReader.readLine();
        if (coords == null) {
            throw new EOFException(
                    "Received an EOF when reading coordinate and there should not be that way."
            );
        }
        return new Coordinate(coords);
    }



    /**
     * Prompt player to input the command to fire or move or use sonar.
     * @param prompt the prompt need to be printed out when reading.
     * @return the command to be implemented.
     * @throws IOException when printing or reading.
     */
    public String readCommand(String prompt) throws IOException {
        out.println(prompt);
        String command = inputReader.readLine();

        if (command == null) {
            throw new EOFException("Received an EOF when reading command.");
        }

        switch (command.trim().toUpperCase()) {
            case "F":
            case "M":
            case "S":
                if ((command.equals("M") && moveRemain == 0) ||
                        (command.equals("S") && sonarRemain == 0)) {
                    throw new IllegalArgumentException("There's no " + command + " remaining, please choose again.");
                }
                return command.toUpperCase();
            default:
                throw new IllegalArgumentException("Invalid command: " + command);
        }
    }


    public void doFirePhase(Board<Character> enemyBoard) throws IOException {
        Coordinate fireCoords = readCoordinateAt("Player " + this.name + ", where do you want to fire?");
        Ship<Character> hitShip = enemyBoard.fireAt(fireCoords);

        if (hitShip == null) {
            out.println("You missed!");
        } else {
            out.println("You hit a " + hitShip.getName() + "!");
        }
    }



    public boolean doMovePhase() throws IOException{
        String movePrompt = "Player " + this.name + " which ship do you want to move(give the coordinate)?";
        Coordinate moveCoords;
        Ship<Character> oldShip;

        // TODO: should prompt again for the coordinate? according to the README,
        //  if the player selects an invalid location, should be re-prompt for actions
        while (true) {
            try {
                moveCoords = readCoordinateAt(movePrompt);
                oldShip = theBoard.getShipAt(moveCoords);
                if(oldShip == null) {
                    throw new IllegalArgumentException("Please choose a coordinate with ship.");
                }
                break; // accept the correct output
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            }
        }

        String shipName = oldShip.getName();
        String moveToPrompt = "Where do you want to place the moved ship?";
        Placement newPlacement;
        try {
            newPlacement = readPlacement(moveToPrompt);
        } catch (IllegalArgumentException e) {
            out.println(e.getMessage());
            return false;
        }
        Ship<Character> newShip = shipCreationFns.get(shipName).apply(newPlacement);
        String errMsg = theBoard.tryAddShip(newShip);
        out.println(errMsg);

        theBoard.tryRemoveShip(oldShip, newShip);

        return true;
    }

    public void doSonarPhase(Board<Character> enemyBoard) throws IOException{
        String movePrompt = "Player " + this.name + " which center do you want to detect (give the coordinate)?";
        Coordinate sonarCoords;

        while (true) {
            try {
                sonarCoords = readCoordinateAt(movePrompt);
                break; // accept the correct output
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            }
        }

        int numSubmarines = 0;
        int numDestroyers = 0;
        int numBattleships = 0;
        int numCarriers = 0;

        for (int i = sonarCoords.getRow() - 1; i <= sonarCoords.getRow() + 1; i++) {
            for (int j = sonarCoords.getColumn() - 1; j <= sonarCoords.getColumn() + 1; j++) {
                Coordinate coord = new Coordinate(i, j);
                Ship<Character> ship = enemyBoard.getShipAt(coord);

                if (ship != null) {
                    if (ship.getName().equals("Submarine")) {
                        numSubmarines++;
                    } else if (ship.getName().equals("Destroyer")) {
                        numDestroyers++;
                    } else if (ship.getName().equals("Battleship")) {
                        numBattleships++;
                    } else if (ship.getName().equals("Carrier")) {
                        numCarriers++;
                    }
                }
            }
        }

        out.println("Submarines occupy " + numSubmarines + " squares");
        out.println("Destroyers occupy " + numDestroyers + " squares");
        out.println("Battleships occupy " + numBattleships + " squares");
        out.println("Carriers occupy " + numCarriers + " squares");
    }


    /**
     * Play one turn game and then change to another player.
     * @param enemyBoard the enemy's board.
     * @param enemyName the name of enemy.
     * @throws IOException when printing or reading.
     */
    public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException{
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        String prompt = "Player " + this.name + "'s turn";
        out.println(prompt);
        out.println(view.displayMyBoardWithEnemyNextToIt(enemyView,
                "Your ocean", "Player " + enemyName + "'s ocean"));
        String commandPrompt = "Possible actions for Player " + this.name +":\n" +
                "\n" +
                " F Fire at a square\n" +
                " M Move a ship to another square (" + moveRemain + "remaining)\n" +
                " S Sonar scan (" + sonarRemain + "remaining)\n" +
                "\n" +
                "Player " + this.name +", what would you like to do?";
        String command;
        while (true) {
            try {
                command = readCommand(commandPrompt);
                break; // accept the correct output
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            }
        }


        if (command.equals("F")) {
            doFirePhase(enemyBoard);
        } else if (command.equals("M")) {
            if (doMovePhase()) {
                out.println("Move Successfully!");
            } else {
                // TODO: redo
                out.println("Move Failed!");
            }
        } else if (command.equals("S")) {
            doSonarPhase(enemyBoard);
        }
    }
}
