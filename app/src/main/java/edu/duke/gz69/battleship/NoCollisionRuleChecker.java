package edu.duke.gz69.battleship;

/**
 * Extends the PlacementRuleChecker and make the rule
 * @param <T>
 */
public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {
    /**
     * Check if the object obey the rule of no collision.
     * @param next the next rule for checking.
     */
    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * check this rule.
     * @param theShip is the ship needs to check if obey the rules.
     * @param theBoard is the board needs to check if obey the rules.
     * @return true if the rule pass.
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for(Coordinate c: theShip.getCoordinates()) {
            if(theBoard.whatIsAtForSelf(c) != null) {
                return "That placement is invalid: the ship overlaps another ship."; // if there is something
            }
        }
        return null;
    }
}
