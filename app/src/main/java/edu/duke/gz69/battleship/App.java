package edu.duke.gz69.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * The entry of whole program
 */
public class App {
    TextPlayer player1;
    TextPlayer player2;
    public App(TextPlayer p1, TextPlayer p2) {
        player1 = p1;
        player2 = p2;
    }

    private static String getStartChoice(BufferedReader inputReader) {
        String input = "";
        boolean validInput = false;
        while (!validInput) {
            try {
                input = inputReader.readLine().trim().toUpperCase();
                if (input.matches("[A-D]")) {
                    validInput = true;
                } else {
                    throw new IllegalArgumentException("Please select a valid option (A-D)");
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        return input;
    }


    /**
     * Create a 10x20 board, create an App with that board, and call
     * doOnePlacement on that app.  For the other arguments to App's constructor,
     * we'll pass new InputStreamReader(System.in) for the input, and pass System.out
     * for the output.
     * @param args nothing
     * @throws IOException when encountering some reading and printing errors
     */
    public static void main(String[] args) throws IOException {
        // placementChecker
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Board<Character> b2 = new BattleShipBoard<>(10, 20, 'X');
        BoardTextView view1 = new BoardTextView(b1);
        BoardTextView view2 = new BoardTextView(b2);
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        V1ShipFactory factory1 = new V1ShipFactory();
        V2ShipFactory factory2 = new V2ShipFactory();

        String prompt = "Please select game mode:\n"+
                "A: human vs human\n" +
                "B: human vs computer\n" +
                "C: computer vs human\n" +
                "D: computer vs computer\n" ;

        System.out.println(prompt);
        String choice = getStartChoice(input);

        TextPlayer p1 = null;
        TextPlayer p2 = null;
        switch(choice) {
            case "A":
                p1 = new TextPlayer("A", b1, view1, input, System.out, factory1, factory2);
                p2 = new TextPlayer("B", b2, view2, input, System.out, factory1, factory2);
                break;
            case "B":
                p1 = new TextPlayer("A", b1, view1, input, System.out, factory1, factory2);
                p2 = new ComputerPlayer("B", b2, view2, input, System.out, factory1, factory2);
                break;
            case "C":
                p1 = new ComputerPlayer("A", b1, view1, input, System.out, factory1, factory2);
                p2 = new TextPlayer("B", b2, view2, input, System.out, factory1, factory2);
                break;
            case "D":
                p1 = new ComputerPlayer("A", b1, view1, input, System.out, factory1, factory2);
                p2 = new ComputerPlayer("B", b2, view2, input, System.out, factory1, factory2);
                break;
            default:
                throw new IllegalArgumentException("Invalid choice: " + choice);
        }

//        TextPlayer p1 = new TextPlayer("A", b1, view1, input, System.out, factory1, factory2);
//        TextPlayer p2 = new TextPlayer("B", b2, view2, input, System.out, factory1, factory2);

        App app = new App(p1, p2);
        app.doPlacementPhase();
        app.doAttackingPhase();
    }

    void doPlacementPhase() throws IOException {
        player1.doPlacementPhase();
        player2.doPlacementPhase();
    }

    public void doAttackingPhase() throws IOException {
        while (true){
            player1.playOneTurn(player2.theBoard, player2.name);
            if (player2.theBoard.checkAllSunk()) {
                System.out.println("Great, Player " + player1.name + " you win!");
                break;
            }
            player2.playOneTurn(player1.theBoard, player1.name);
            if (player1.theBoard.checkAllSunk()) {
                System.out.println("Great, Player " + player2.name + " you win!");
                break;
            }
        }
    }
}
