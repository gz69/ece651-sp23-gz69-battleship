package edu.duke.gz69.battleship;

import java.util.ArrayList;

/**
 * This interface represents any type of Ship in our BattleShip game. It is generic
 * in typename T, which is the type of information the view needs to display this ship.
 */
public interface Ship<T> {

    /**
     * Get all the Coordinates that this Ship occupies.
     * @return An Iterable with the coordinates that this Ship occupies
     */
    Iterable<Coordinate> getCoordinates();

    /**
     * Get the name of this Ship, such as "submarine".
     * @return the name of this ship
     */
    String getName();
    /**
     * Check if this ship occupies the given coordinate.
     * @param where is the Coordinate to check if this Ship occupies
     * @return true if where is inside this ship, false if not.
     */
    boolean occupiesCoordinates(Coordinate where);

    /**
     * Check if this ship has been hit in all of its locations meaning it has been sunk.
     *
     * @return true if this ship has been sunk, false otherwise.
     */
    boolean isSunk();

    /**
     * Make this ship record that is has been hit at the given coordinate. The specified
     * coordinate must be part of the ship.
     * @param where specifies the coordinates that were hit.
     * @throws IllegalArgumentException if where is not part of the Ship.
     */
    void recordHitAt(Coordinate where);

    /**
     * Check if this ship was hit at the specified coordinate. The specified
     * coordinate must be part of the ship.
     * @param where is the coordinates to check.
     * @return true if the ship was hit at the indicated coordinates, and false otherwise.
     * @throws IllegalArgumentException if where is not part of the Ship.
     */
    boolean wasHitAt(Coordinate where);

    ArrayList<Integer> getHitIndexes();
    void recordMovedHits(ArrayList<Integer> hitIndexes);
    /**
     * Return the view-specific information at the given coordinate. This coordinate must
     * be part of the ship.
     * @param where is the coordinate to return information.
     * @param myShip is true if display my own ship, false the enemies.
     * @throws IllegalArgumentException if where is not part of the Ship.
     * @return the view-specific information at the coordinate.
     */
    T getDisplayInfoAt(Coordinate where, boolean myShip);
}
