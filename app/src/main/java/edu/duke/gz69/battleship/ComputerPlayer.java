package edu.duke.gz69.battleship;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.util.Random;

public class ComputerPlayer extends TextPlayer{
    private final Random random;
    public ComputerPlayer(String name, Board<Character> board, BoardTextView view, BufferedReader inputReader,
                          PrintStream out, V1ShipFactory shipFactory1, V2ShipFactory shipFactory2) {
        super(name, board, view, inputReader, out, shipFactory1, shipFactory2);
        this.random = new Random();
    }

    @Override
    public void doOnePlacement(String shipName) {

        while (true) {
            try {
                String s;
                String row = String.valueOf((char)(this.random.nextInt(20) + 65));
                String col = String.valueOf(this.random.nextInt(10));
                int randomOrientation = this.random.nextInt(10);
                if (randomOrientation % 3 == 0) {
                    s = row + col + "H";
                } else if(randomOrientation % 3 == 1){
                    s = row + col + "U";
                } else {
                    s = row + col + "L";
                }
                Placement p = new Placement(s);
                Ship<Character> ship = shipCreationFns.get(shipName).apply(p);
                String errMsg = theBoard.tryAddShip(ship);

                if (errMsg == null) {
                    break; // accept the correct output
                } else {
                    out.println(errMsg);
                }
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            }

        }

        out.println("Current ocean:");
        out.print(view.displayMyOwnBoard());
    }

    @Override
    public void playOneTurn(Board<Character> enemyBoard, String enemyName){
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        String prompt = String.format("Player %s's turn (Computer)\n%s",
                this.name,
                view.displayMyBoardWithEnemyNextToIt(enemyView,
                        "Your ocean",
                        "Player " + enemyName + "'s ocean"));
        out.println(prompt);
        doFirePhase(enemyBoard);
    }


    @Override
    public void doFirePhase(Board<Character> enemyBoard) {
        Coordinate fireCoords;
        while (true) {
            try {
                int row = (int) (Math.random() * 20) + 65;
                int col = (int) (Math.random() * 10);
                fireCoords = new Coordinate(Character.toString((char) row) + col);
                break;
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            }
        }

        Ship<Character> hitShip = enemyBoard.fireAt(fireCoords);
        if (hitShip == null) {
            out.println("Computer " + name + " missed!");
        } else {
            out.println("Computer " + name + " hit a " + hitShip.getName());
        }
    }

}
