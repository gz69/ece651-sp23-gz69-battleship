package edu.duke.gz69.battleship;

public abstract class PlacementRuleChecker<T> {
    private final PlacementRuleChecker<T> next;

    /**
     * Check the rule chain
     * @param next the next rule to be pass
     */
    protected PlacementRuleChecker(PlacementRuleChecker<T> next) {
        this.next = next;
    }

    /**
     * Subclasses will implement their own rules.
     * @param theShip is the ship needs to check if obey the rules.
     * @param theBoard is the board needs to check if obey the rules.
     * @return true if pass the rule, otherwise false.
     */
    protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

    /**
     * Note that we use tail recursion, but the "this" is changing with each recursive call,
     * so we make progress down the chain.  Subclasses will generally NOT override
     * this method.
     * @param theShip is the ship needs to check if obey the rules.
     * @param theBoard is the board needs to check if obey the rules.
     * @return true if all rules are passed or false if not.
     */
    public String checkPlacement (Ship<T> theShip, Board<T> theBoard) {
        //if we fail our own rule: stop the placement is not legal
        String msg = checkMyRule(theShip, theBoard);
        if (msg != null) {
            return msg;
        }
        //otherwise, ask the rest of the chain.
        if (next != null) {
            return next.checkPlacement(theShip, theBoard);
        }
        //if there are no more rules, then the placement is legal
        return null;
    }
}
