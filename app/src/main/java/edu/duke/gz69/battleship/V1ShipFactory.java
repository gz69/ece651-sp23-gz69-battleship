package edu.duke.gz69.battleship;

/**
 * implements the ship factory to make 4 kinds of ships
 */
public class V1ShipFactory implements AbstractShipFactory<Character> {

    /**
     * Create ships with rectangle ships.
     * @param where the place to put the ship.
     * @param w the width of the ship.
     * @param h the height.
     * @param letter the ship's presentation.
     * @param name the name of this kind of ship.
     * @return the Ship be created.
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        return new RectangleShip<>(name, where.getWhere(), w, h, letter, '*');
    }
    @Override

    public Ship<Character> makeSubmarine(Placement where) {
        int width = (where.getOrientation() == 'V') ? 1 : 2;
        int height = (where.getOrientation() == 'V') ? 2 : 1;
        return createShip(where, width, height, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        if(where.getOrientation() == 'V') {
            return createShip(where, 1, 3, 'd', "Destroyer");
        } else {
            return createShip(where, 3, 1, 'd', "Destroyer");
        }
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        if(where.getOrientation() == 'V') {
            return createShip(where, 1, 4, 'b', "Battleship");
        } else {
            return createShip(where, 4, 1, 'b', "Battleship");
        }
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        if(where.getOrientation() == 'V') {
            return createShip(where, 1, 6, 'c', "Carrier");
        } else {
            return createShip(where, 6, 1, 'c', "Carrier");
        }
    }
}
