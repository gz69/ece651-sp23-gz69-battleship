package edu.duke.gz69.battleship;

/**
 * This interface represents an Abstract Factory patter for Ship creation
 * @param <T> the type of ship
 */
public interface AbstractShipFactory<T> {
    /**
     * Make a submarine.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the submarine;
     */
    Ship<T> makeSubmarine(Placement where);

    /**
     * Make a submarine.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the submarine;
     */
    Ship<T> makeBattleship(Placement where);

    /**
     * Make a submarine.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the submarine;
     */
    Ship<T> makeCarrier(Placement where);

    /**
     * Make a submarine.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the submarine;
     */
    Ship<T> makeDestroyer(Placement where);


}

