package edu.duke.gz69.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of a Board (i.e., converting it to a
 * string to show to the user).
 * It supports two ways to display the Board:
 * one for the player's own board,
 * one for the enemy's board.
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;

    /**
     * Construct a BoardView, given the board it will display.
     * @param toDisplay is the board to display
     * @throws IllegalArgumentException if the board is larger than 10 x 26.
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        checkBoardSize(toDisplay);
    }

    private void checkBoardSize(Board<Character> board) {
        int width = board.getWidth();
        int height = board.getHeight();
        if (width > 10 || height > 26) {
            throw new IllegalArgumentException(String.format(
                    "Board must be no larger than 10x26, but is %dx%d", width, height));
        }
    }


    /**
     * This makes the view of my own board, e.g.
     *   0|1|2|3|4|5|6|7|8|9
     * A  | | | | | | | | |  A
     * B  | | | | | | | | |  B
     * C  | | | | | | | | |  C
     * D  | | | | | | | | |  D
     * E  | | | | | | | | |  E
     * F  | | | | | | | | |  F
     * G  | | | | | | | | |  G
     * H  | | | | | | | | |  H
     * I  | | | | | | | | |  I
     * J  | | | | | | | | |  J
     * K  | | | | | | | | |  K
     * L  | | | | | | | | |  L
     * M  | | | | | | | | |  M
     * N  | | | | | | | | |  N
     * O  | | | | | | | | |  O
     * P  | | | | | | | | |  P
     * Q  | | | | | | | | |  Q
     * R  | | | | | | | | |  R
     * S  | | | | | | | | |  S
     * T  | | | | | | | | |  T
     *   0|1|2|3|4|5|6|7|8|9
     * @return the String of my own board
     */
    public String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        int width = toDisplay.getWidth();
        int height = toDisplay.getHeight();
        String header = makeHeader();
        StringBuilder board = new StringBuilder();
        board.append(header);

        for (int row = 0; row < height; row++) {
            char rowChar = (char) ('A' + row);
            String leftBound = rowChar + " ";
            board.append(leftBound);

            for (int col = 0; col < width; col++) {
                Coordinate coord = new Coordinate(row, col);
                Character elem = getSquareFn.apply(coord);
                board.append(elem == null ? ' ' : elem);
                board.append("|");
            }

            board.setLength(board.length() - 1); // remove last separator
            String rightBound = " " + rowChar + "\n";
            board.append(rightBound);
        }

        board.append(header);
        return board.toString();
    }


    public String displayMyOwnBoard() {
        return displayAnyBoard(toDisplay::whatIsAtForSelf);
    }

    public String displayEnemyBoard() {
        return displayAnyBoard(toDisplay::whatIsAtForEnemy);
    }

    /**
     * This makes the header line e.g. 0|1|2|3|4\n
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder header = new StringBuilder("  "); // two spaces to align with board
        String separator = "";
        for (int col = 0; col < toDisplay.getWidth(); col++) {
            header.append(separator);
            header.append(col);
            separator = "|";
        }
        header.append("  \n"); // two spaces and a newline to align with board
        return header.toString();
    }


    public String space(int k){
        return " ".repeat(k);
    }

    /**
     * Display both the player's board but also enemy's.
     * @param enemyView the enemy's board view.
     * @param myHeader the header of current player to be print.
     * @param enemyHeader the header of enemy to be print.
     * @return String that be concat.
     */
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView,
                                                  String myHeader, String enemyHeader) {
        String enemyBoard  = enemyView.displayEnemyBoard();
        String myBoard = displayMyOwnBoard();

        int myWidth = toDisplay.getWidth();

        String[] myBoardArr = myBoard.split("\n");
        String[] enemyBoardArr = enemyBoard.split("\n");

        int headerStart = 5;
        int headerDistance = 2 * myWidth + 22 - headerStart - myHeader.length();
        int enemyBoardSpace = 16;
        int boardHeaderSpace = enemyBoardSpace + 2;

        StringBuilder allBoard = new StringBuilder();

        allBoard.append('\n');
        allBoard.append(myHeader);
        allBoard.append(space(headerDistance));
        allBoard.append(enemyHeader);
        allBoard.append('\n');

        allBoard.append(myBoardArr[0]);
        allBoard.append(space(boardHeaderSpace));
        allBoard.append(enemyBoardArr[0]);
        allBoard.append('\n');

        /*start to tackle the file itself*/
        for(int i = 1; i < myBoardArr.length - 1; i++){
            allBoard.append(myBoardArr[i]);
            allBoard.append(space(enemyBoardSpace));
            allBoard.append(enemyBoardArr[i]);
            allBoard.append("\n");
        }

        allBoard.append(myBoardArr[myBoardArr.length - 1]);
        allBoard.append(space(boardHeaderSpace));
        allBoard.append(enemyBoardArr[enemyBoardArr.length - 1]);
        allBoard.append('\n');

        return allBoard.toString();
    }
}
