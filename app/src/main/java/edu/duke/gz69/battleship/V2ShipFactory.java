package edu.duke.gz69.battleship;

/**
 * implements the ship factory to make 4 kinds of ships
 */
public class V2ShipFactory extends V1ShipFactory {

    /**
     * Create ships with rectangle ships.
     * @param where the place to put the ship.
     * @param letter the ship's presentation.
     * @param name the name of this kind of ship.
     * @return the Ship be created.
     */
    protected Ship<Character> createShip(Placement where, char letter, String name) {
        switch (name) {
            case "Battleship":
                return new TShapeShip<>(name, where, letter, '*');
            case "Carrier":
                return new ZShapeShip<>(name, where, letter, '*');
            default:
                throw new IllegalArgumentException("Should create battle ship or carriers but received " + name);
        }
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 'c', "Carrier");
    }
}
