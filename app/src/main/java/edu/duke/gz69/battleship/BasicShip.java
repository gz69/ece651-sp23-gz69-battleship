package edu.duke.gz69.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * An abstract class and implement Ship<T>.
 * @param <T> Character or Images, it depends.
 */
public abstract class BasicShip<T> implements Ship<T>{
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected HashMap<Coordinate, Boolean> myPieces;
    protected ShipDisplayInfo<T> enemyDisplayInfo;
    protected HashMap<Coordinate, Integer> indexedPieces;

    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }

    /**
     * Construct a basic ship with the Coordinates and the Display info
     * @param wheres is an Iterable set of Coordinates this Ship occupies
     * @param myDisplayInfo is a member to help check what is the info in the Coordinate
     */
    public BasicShip(Iterable<Coordinate> wheres, ShipDisplayInfo<T> myDisplayInfo,
                     ShipDisplayInfo<T> enemyDisplayInfo, HashMap<Coordinate, Integer> indexedPieces) {
        myPieces = new HashMap<>();
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        for(Coordinate c: wheres) {
            myPieces.put(c, false);
        }
        this.indexedPieces = indexedPieces;
    }

    /**
     * Check if the Coordinates was occupied by a ship
     * @param where is the Coordinate to check if this Ship occupies
     * @return false if the Coordinates was occupied by a (ship in the map), true otherwise
     */
    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        return myPieces.containsKey(where);
    }

    /**
     * Check if the ship is sunk
     * @return true if the ship is sunk
     */
    @Override
    public boolean isSunk() {
        return myPieces.values().stream().allMatch(Boolean::booleanValue);
    }

    /**
     * Record the where the hit is on ship
     * @param where specifies the coordinates that were hit.
     */
    @Override
    public void recordHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        myPieces.replace(where, true);
    }

    /**
     * Check the hit states of specified Coordinate.
     * @param where is the coordinates to check.
     * @return true if the coordinate has a hit, otherwise false.
     */
    @Override
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        return myPieces.getOrDefault(where, false);
    }

    /**
     * Look up the hit status of this coordinate.
     * @param where is the coordinate to return information.
     * @return the info in that Coordinate
     */
    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) {
        checkCoordinateInThisShip(where);

        if (myShip) {
            return myDisplayInfo.getInfo(where, myPieces.get(where));
        } else {
            return enemyDisplayInfo.getInfo(where, myPieces.get(where));
        }
    }

    /**
     * we said (in our documentation in Ship.java) that the three methods that take a Coordinate
     * throw an IllegalArgumentException if the Coordinate is not
     * in this ship.  We may as well go ahead and write
     * a protected helper method to abstract out this checking/exception throwing.
     * @param c is the coordinate need to be checked.
     */
    protected void checkCoordinateInThisShip(Coordinate c) {
        if (!myPieces.containsKey(c)) {
            String message = String.format("(%d,%d) is not a Coordinate on this ship!", c.getRow(), c.getColumn());
            throw new IllegalArgumentException(message);
        }
    }


    public ArrayList<Integer> getHitIndexes() {
        ArrayList<Integer> hitIndexes = new ArrayList<>();
        for (Coordinate coord : myPieces.keySet()) {
            if (myPieces.get(coord)) {
                hitIndexes.add(indexedPieces.get(coord));
            }
        }
        return hitIndexes;
    }


    public void recordMovedHits(ArrayList<Integer> hitIndexes) {
        for (int i = 0; i < hitIndexes.size(); i++) {
            Integer hitIndex = hitIndexes.get(i);
            Coordinate hitCoordinate = null;
            for (Coordinate coord : indexedPieces.keySet()) {
                if (indexedPieces.get(coord).equals(hitIndex)) {
                    hitCoordinate = coord;
                    break;
                }
            }
            if (hitCoordinate != null) {
                recordHitAt(hitCoordinate);
            }
        }
    }

}
