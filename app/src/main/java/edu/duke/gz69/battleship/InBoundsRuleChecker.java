package edu.duke.gz69.battleship;

/**
 * Extends the PlacementRuleChecker and make the rule
 * @param <T>
 */
public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T>{
    /**
     * Check if the object obey the rule inbound.
     * @param next the next rule for checking.
     */
    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * check this rule.
     * @param theShip is the ship needs to check if obey the rules.
     * @param theBoard is the board needs to check if obey the rules.
     * @return true if the rule pass.
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate c : theShip.getCoordinates()) {
            if (!isWithinBounds(c, theBoard)) {
                return String.format("That placement is invalid: the ship goes off the %s of the board.", getBoundsDirection(c, theBoard));
            }
        }
        return null;
    }

    private boolean isWithinBounds(Coordinate c, Board<T> theBoard) {
        int width = theBoard.getWidth();
        int height = theBoard.getHeight();
        return c.getRow() >= 0 && c.getRow() < height && c.getColumn() >= 0 && c.getColumn() < width;
    }

    private String getBoundsDirection(Coordinate c, Board<T> theBoard) {
        int width = theBoard.getWidth();
        int height = theBoard.getHeight();
        if (c.getRow() < 0) {
            return "top";
        } else if (c.getRow() >= height) {
            return "bottom";
        } else if (c.getColumn() < 0) {
            return "left";
        } else if (c.getColumn() >= width) {
            return "right";
        } else {
            return "unknown";
        }
    }

}
