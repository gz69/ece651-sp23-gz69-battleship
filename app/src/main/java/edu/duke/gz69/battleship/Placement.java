package edu.duke.gz69.battleship;

public class Placement {
    private final Coordinate where;
    private final char orientation;

    /**
     * Construct a Placement with the specified coordinate and orientation
     * @param where is the index of the coordinate
     * @param orientation is the direction of this newly constructed Placement
     */
    public Placement(Coordinate where, char orientation) {
        orientation = Character.toUpperCase(orientation);
        if (orientation != 'V' && orientation != 'H') {
            throw new IllegalArgumentException("Orientation must be 'V' or 'H', but is " + orientation);
        }
        this.where = where;
        this.orientation = orientation;
    }


    /**
     * Construct a Placement with the specified coordinate and orientation
     * @param desc is a string like "A2V"
     */
    public Placement(String desc) {
        if (desc.length() != 3) {
            throw new IllegalArgumentException("Placement string must be of length 3, but is " + desc.length());
        }

        Coordinate where = new Coordinate(desc.substring(0, 2));

        char orientation = Character.toUpperCase(desc.charAt(2));
        switch (orientation) {
            case 'V':
            case 'H':
            case 'U':
            case 'D':
            case 'L':
            case 'R':
                break;
            default:
                throw new IllegalArgumentException("Invalid orientation: " + orientation);
        }

        this.where = where;
        this.orientation = orientation;
    }


    public char getOrientation() {
        return orientation;
    }

    public Coordinate getWhere() {
        return where;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Placement)) {
            return false;
        }

        Placement other = (Placement) o;

        return this.where.equals(other.where) && this.orientation == other.orientation;
    }


    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Placement{")
                .append("where=").append(where)
                .append(", orientation='").append(orientation).append('\'')
                .append('}');
        return sb.toString();
    }
}
