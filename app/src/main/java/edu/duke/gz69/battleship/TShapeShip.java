package edu.duke.gz69.battleship;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Battleships' new style
 * @param <T>
 */
public class TShapeShip<T> extends BasicShip<T> {
    private final String name;
    static HashSet<Coordinate> generateCoords(Coordinate upperLeft, int width, int height){
        HashSet<Coordinate> coords = new HashSet<>();
        for(int i = 0; i < height; i++) {
            for(int j = 0; j < width; j++) {
                coords.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + j));
            }
        }
        return coords;
    }

    /**
     * Make all the set of coordinates for a battleship starting
     * at upperLeft like
     *               *b      OR    B         Bbb        *b
     *               bbb           bb   OR    b     OR  bb
     *                             b                     b
     *                Up          Right      Down      Left
     * @param place the start point of the ship
     * @return the set of Coordinates, e.g. set {(row=1,col=2), (row=2,col=2), (row=3,col=2)}
     */
    static HashSet<Coordinate> makeCoords(Placement place) {
        Coordinate upperLeft = place.getWhere();
        Character orientation = place.getOrientation();
        int row = upperLeft.getRow();
        int col = upperLeft.getColumn();
        HashSet<Coordinate> shipCoords = new HashSet<>();
        switch (orientation) {
            case 'U':
                shipCoords.addAll(generateCoords(new Coordinate(row - 1, col), 3, 1));
                shipCoords.add(new Coordinate(row, col));
                break;
            case 'R':
                shipCoords.addAll(generateCoords(new Coordinate(row, col), 1, 3));
                shipCoords.add(new Coordinate(row, col + 1));
                break;
            case 'D':
                shipCoords.addAll(generateCoords(new Coordinate(row + 1, col), 3, 1));
                shipCoords.add(new Coordinate(row + 3, col + 2));
                break;
            case 'L':
                shipCoords.addAll(generateCoords(new Coordinate(row, col - 1), 1, 3));
                shipCoords.add(new Coordinate(row + 2, col));
                break;
            default:
                throw new IllegalArgumentException(
                        "Invalid orientation: " + orientation
                );
        }
        return shipCoords;
    }

    static HashMap<Coordinate, Integer> makeMaps(Placement place) {
        Coordinate upperLeft = place.getWhere();
        Character orientation = place.getOrientation();
        int row = upperLeft.getRow();
        int col = upperLeft.getColumn();
        HashMap<Coordinate, Integer> shipMaps = new HashMap<>();

        switch(orientation) {
            case 'U':
                shipMaps.put(new Coordinate(row, col + 1), 1);
                shipMaps.put(new Coordinate(row + 1, col), 2);
                shipMaps.put(new Coordinate(row + 1, col + 1), 3);
                shipMaps.put(new Coordinate(row + 1, col + 2), 4);
                break;
            case 'R':
                shipMaps.put(new Coordinate(row + 1, col + 1), 1);
                shipMaps.put(new Coordinate(row, col), 2);
                shipMaps.put(new Coordinate(row + 1, col), 3);
                shipMaps.put(new Coordinate(row + 2, col), 4);
                break;
            case 'D':
                shipMaps.put(new Coordinate(row + 1, col + 1), 1);
                shipMaps.put(new Coordinate(row, col + 2), 2);
                shipMaps.put(new Coordinate(row, col + 1), 3);
                shipMaps.put(new Coordinate(row, col), 4);
                break;
            case 'L':
                shipMaps.put(new Coordinate(row + 1, col), 1);
                shipMaps.put(new Coordinate(row, col + 1), 4);
                shipMaps.put(new Coordinate(row + 1, col + 1), 3);
                shipMaps.put(new Coordinate(row + 2, col + 1), 2);
                break;
            default:
                throw new IllegalArgumentException("Invalid orientation: " + orientation);
        }
        return shipMaps;
    }


    /**
     * Construct a T shape ship with the Coordinates and the Display info
     *
     * @param place           is an Iterable set of Coordinates this Ship occupies
     * @param selfDisplayInfo    is a member to help check what is the info in the Coordinate
     * @param enemyDisplayInfo is a member to help check what is the info in the Coordinate with enemy's view
     * @param name is the name of ship
     */
    public TShapeShip(String name, Placement place,
                      SimpleShipDisplayInfo<T> selfDisplayInfo, SimpleShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(place), selfDisplayInfo, enemyDisplayInfo, makeMaps(place));
        this.name = name;
    }

    public TShapeShip(String name, Placement p, T data, T onHit) {
        super(makeCoords(p), new SimpleShipDisplayInfo<>(data, onHit), new SimpleShipDisplayInfo<>(null, data), makeMaps(p));
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
