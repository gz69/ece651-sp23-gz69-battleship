package edu.duke.gz69.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class V2ShipFactoryTest {

    @Test
    void test_make_different_ships() {
        V1ShipFactory shipFactory = new V2ShipFactory();

        Ship<Character> bat_v = shipFactory.makeBattleship(new Placement("A0U"));
        checkShip(bat_v,"Battleship",'b',
                new Coordinate(0, 1),
                new Coordinate(1, 0),
                new Coordinate(1, 1),
                new Coordinate(1, 2)
        );

        Ship<Character> bat_v3 = shipFactory.makeBattleship(new Placement("A0D"));
        checkShip(bat_v3,"Battleship",'b',
                new Coordinate(0, 0),
                new Coordinate(0, 1),
                new Coordinate(0, 2),
                new Coordinate(1, 1)
        );

        Ship<Character> carr = shipFactory.makeCarrier(new Placement("A1D"));
        checkShip(carr,"Carrier",'c',
                new Coordinate(0, 1),
                new Coordinate(1, 1),
                new Coordinate(2, 1),
                new Coordinate(1, 2),
                new Coordinate(2, 2),
                new Coordinate(3, 2),
                new Coordinate(4, 2)
        );

        Ship<Character> carr2 = shipFactory.makeCarrier(new Placement("A0R"));
        checkShip(carr2,"Carrier",'c',
                new Coordinate(0, 1),
                new Coordinate(0, 2),
                new Coordinate(0, 3),
                new Coordinate(0, 4),
                new Coordinate(1, 0),
                new Coordinate(1, 1),
                new Coordinate(1, 2)
        );

        assertThrows(IllegalArgumentException.class, () -> shipFactory.makeCarrier(new Placement("A0H")));
    }

    private void checkShip(Ship<Character> testShip, String expectedName,
                           char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(testShip.getName(),expectedName);

        for(Coordinate c:expectedLocs){
            assertEquals(testShip.getDisplayInfoAt(c, true),expectedLetter);
        }
    }
}