package edu.duke.gz69.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ZShapeShipTest {

    @Test
    void test_get_name() {
        SimpleShipDisplayInfo<Character> myDisplayInfo = new SimpleShipDisplayInfo<>('s', '*');
        SimpleShipDisplayInfo<Character> enemyDisplayInfo = new SimpleShipDisplayInfo<>(' ', 's');

        Ship<Character> carrier = new ZShapeShip<>("Carrier", new Placement("A0U"), myDisplayInfo, enemyDisplayInfo);
        assertEquals("Carrier", carrier.getName());
    }
}