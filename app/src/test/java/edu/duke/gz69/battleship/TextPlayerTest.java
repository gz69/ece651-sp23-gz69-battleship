package edu.duke.gz69.battleship;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class TextPlayerTest {

    private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<>(w, h, 'X');
        BoardTextView view = new BoardTextView(board);
        V1ShipFactory shipFactory = new V1ShipFactory();
        V2ShipFactory shipFactory2 = new V2ShipFactory();
        return new TextPlayer("A", board, view, input, output, shipFactory, shipFactory2);
    }

    @Test
    void test_read_placement() throws IOException {
        StringReader sr = new StringReader("B2V\nC8H\na4v\n");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(bytes, true);
        Board<Character> b = new BattleShipBoard<>(9,19, 'X');
        BoardTextView view = new BoardTextView(b);

        TextPlayer player = new TextPlayer("A", b, view,
                new BufferedReader(sr), ps, new V1ShipFactory(), new V2ShipFactory());
        String prompt = "Please enter a location for a ship:";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1,2), 'V');
        expected[1] = new Placement(new Coordinate(2,8), 'H');
        expected[2] = new Placement(new Coordinate(0,4), 'V');

        for (Placement placement : expected) {
            Placement p = player.readPlacement(prompt);
            assertEquals(p, placement);
            assertEquals(prompt + "\n", bytes.toString());
            bytes.reset(); // for the next test case
        }
    }

    @Test
    public void test_one_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        // NOTICE here will be failed
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC7H\na4v\n", bytes);

        String prompt = "Player A where do you want to place a Destroyer?\n" +
                "Current ocean:";
        String[] expected = new String[3];
        expected[0] = prompt + "\n" +
                "  0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | | | | | | |  A\n" +
                "B  | |d| | | | | | |  B\n" +
                "C  | |d| | | | | | |  C\n" +
                "D  | |d| | | | | | |  D\n" +
                "E  | | | | | | | | |  E\n" +
                "F  | | | | | | | | |  F\n" +
                "G  | | | | | | | | |  G\n" +
                "H  | | | | | | | | |  H\n" +
                "I  | | | | | | | | |  I\n" +
                "J  | | | | | | | | |  J\n" +
                "K  | | | | | | | | |  K\n" +
                "L  | | | | | | | | |  L\n" +
                "M  | | | | | | | | |  M\n" +
                "N  | | | | | | | | |  N\n" +
                "O  | | | | | | | | |  O\n" +
                "P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9\n"   ;

        expected[1] =
                prompt + "\n" +
                        "  0|1|2|3|4|5|6|7|8|9\n" +
                        "A  | | | | | | | | |  A\n" +
                        "B  | |d| | | | | | |  B\n" +
                        "C  | |d| | | | |d|d|d C\n" +
                        "D  | |d| | | | | | |  D\n" +
                        "E  | | | | | | | | |  E\n" +
                        "F  | | | | | | | | |  F\n" +
                        "G  | | | | | | | | |  G\n" +
                        "H  | | | | | | | | |  H\n" +
                        "I  | | | | | | | | |  I\n" +
                        "J  | | | | | | | | |  J\n" +
                        "K  | | | | | | | | |  K\n" +
                        "L  | | | | | | | | |  L\n" +
                        "M  | | | | | | | | |  M\n" +
                        "N  | | | | | | | | |  N\n" +
                        "O  | | | | | | | | |  O\n" +
                        "P  | | | | | | | | |  P\n" +
                        "Q  | | | | | | | | |  Q\n" +
                        "R  | | | | | | | | |  R\n" +
                        "S  | | | | | | | | |  S\n" +
                        "T  | | | | | | | | |  T\n" +
                        "  0|1|2|3|4|5|6|7|8|9\n"   ;

        expected[2] = prompt + "\n" +
                "  0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | |d| | | | |  A\n" +
                "B  | |d| |d| | | | |  B\n" +
                "C  | |d| |d| | |d|d|d C\n" +
                "D  | |d| | | | | | |  D\n" +
                "E  | | | | | | | | |  E\n" +
                "F  | | | | | | | | |  F\n" +
                "G  | | | | | | | | |  G\n" +
                "H  | | | | | | | | |  H\n" +
                "I  | | | | | | | | |  I\n" +
                "J  | | | | | | | | |  J\n" +
                "K  | | | | | | | | |  K\n" +
                "L  | | | | | | | | |  L\n" +
                "M  | | | | | | | | |  M\n" +
                "N  | | | | | | | | |  N\n" +
                "O  | | | | | | | | |  O\n" +
                "P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9\n"   ;

        for (String s : expected) {
            player.doOnePlacement("Destroyer");
            assertEquals(s, bytes.toString());
            bytes.reset();
        }
    }

    @Test
    void test_do_placement_phase()   {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        // NOTICE here will be failed
        TextPlayer player = createTextPlayer(9, 19, "B2V\n", bytes);
//        String expected = "  0|1|2|3|4|5|6|7|8|9\n" +
//                "A  | | | | | | | | |  A\n" +
//                "B  | | | | | | | | |  B\n" +
//                "C  | | | | | | | | |  C\n" +
//                "D  | | | | | | | | |  D\n" +
//                "E  | | | | | | | | |  E\n" +
//                "F  | | | | | | | | |  F\n" +
//                "G  | | | | | | | | |  G\n" +
//                "H  | | | | | | | | |  H\n" +
//                "I  | | | | | | | | |  I\n" +
//                "J  | | | | | | | | |  J\n" +
//                "K  | | | | | | | | |  K\n" +
//                "L  | | | | | | | | |  L\n" +
//                "M  | | | | | | | | |  M\n" +
//                "N  | | | | | | | | |  N\n" +
//                "O  | | | | | | | | |  O\n" +
//                "P  | | | | | | | | |  P\n" +
//                "Q  | | | | | | | | |  Q\n" +
//                "R  | | | | | | | | |  R\n" +
//                "S  | | | | | | | | |  S\n" +
//                "T  | | | | | | | | |  T\n" +
//                "  0|1|2|3|4|5|6|7|8|9\n" +
//                "--------------------------------------------------------------------------------\n" +
//                "Player A: you are going to place the following ships (which are all\n" +
//                "rectangular). For each ship, type the coordinate of the upper left\n" +
//                "side of the ship, followed by either H (for horizontal) or V (for\n" +
//                "vertical).  For example M4H would place a ship horzontally starting\n" +
//                "at M4 and going to the right.  You have\n" +
//                "\n" +
//                "2 \"Submarines\" ships that are 1x2 \n" +
//                "3 \"Destroyers\" that are 1x3\n" +
//                "3 \"Battleships\" that are 1x4\n" +
//                "2 \"Carriers\" that are 1x6\n" +
//                "--------------------------------------------------------------------------------\n" +
//                "\n" +
//                "Player A where would you like to put your ship?\n" +
//                "  0|1|2|3|4|5|6|7|8|9\n" +
//                "A  | | | | | | | | |  A\n" +
//                "B  | |d| | | | | | |  B\n" +
//                "C  | |d| | | | | | |  C\n" +
//                "D  | |d| | | | | | |  D\n" +
//                "E  | | | | | | | | |  E\n" +
//                "F  | | | | | | | | |  F\n" +
//                "G  | | | | | | | | |  G\n" +
//                "H  | | | | | | | | |  H\n" +
//                "I  | | | | | | | | |  I\n" +
//                "J  | | | | | | | | |  J\n" +
//                "K  | | | | | | | | |  K\n" +
//                "L  | | | | | | | | |  L\n" +
//                "M  | | | | | | | | |  M\n" +
//                "N  | | | | | | | | |  N\n" +
//                "O  | | | | | | | | |  O\n" +
//                "P  | | | | | | | | |  P\n" +
//                "Q  | | | | | | | | |  Q\n" +
//                "R  | | | | | | | | |  R\n" +
//                "S  | | | | | | | | |  S\n" +
//                "T  | | | | | | | | |  T\n" +
//                "  0|1|2|3|4|5|6|7|8|9\n";
        assertThrows(EOFException.class, player::doPlacementPhase);
//        assertEquals(expected, bytes.toString());
    }

    @Test
    void test_move_phase() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        // NOTICE here will be failed
        TextPlayer player = createTextPlayer(5, 5, "A0U\n", bytes);
        player.doOnePlacement("Battleship");
        String prompt = "Player A where do you want to place a Battleship?\n" +
                "Current ocean:";
        String expected = prompt + "\n" +
                "  0|1|2|3|4\n" +
                "A  |b| | |  A\n" +
                "B b|b|b| |  B\n" +
                "C  | | | |  C\n" +
                "D  | | | |  D\n" +
                "E  | | | |  E\n" +
                "  0|1|2|3|4\n";
        assertEquals(expected, bytes.toString());
        Ship<Character> s = player.theBoard.getShipAt(new Coordinate("A1"));
        assertNotNull(s);
        s.recordHitAt(new Coordinate("b1"));
        Ship<Character> s2 = new V2ShipFactory().makeBattleship(new Placement("d2d"));
        assertNotNull(s2);

        String Removed = player.theBoard.tryRemoveShip(s, s2);
        assertNull(Removed); // remove successfully
//        player.doMovePhase();
        assertEquals('b', player.theBoard.whatIsAtForSelf(new Coordinate("d2")));
        assertEquals('*', player.theBoard.whatIsAtForSelf(new Coordinate("d3")));

//        assertEquals(expected, );
//        assertNull(s);

    }

}