package edu.duke.gz69.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlacementTest {

    @Test
    public void test_valid_arguments() {
        Coordinate where = new Coordinate(1,2);

        Placement p1 = new Placement(where, 'V');
        Placement p2 = new Placement(where, 'H');

        assertEquals(p1.getOrientation(), 'V');
        assertEquals(p2.getOrientation(), 'H');
    }

    @Test
    public void test_invalid_arguments() {
        Coordinate where = new Coordinate(1,2);

        assertThrows(IllegalArgumentException.class, () -> new
                Placement(where, 'x'));
        assertThrows(IllegalArgumentException.class, () -> new
                Placement(where, '@'));
    }

    @Test
    public void test_to_string() {
        Coordinate where = new Coordinate(1,2);
        Placement p1 = new Placement(where, 'H');
        Placement p2 = new Placement(where, 'V');
        assertEquals(p1.toString(), "(1, 2) Horizontal");
        assertEquals(p2.toString(), "(1, 2) Vertical");
    }

    @Test
    public void test_equals() {
        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(1,2);

        Placement p1 = new Placement(c1, 'H');
        Placement p2 = new Placement(c1, 'V');
        Placement p3 = new Placement(c1, 'H');
        Placement p4 = new Placement(c2, 'H');

        assertEquals(p1, p1);
        assertEquals(p1, p3);
        assertEquals(p1, p4);

        assertNotEquals(p1, p2);
        assertNotEquals(p1, "(1, 2) Horizontal");
    }

    @Test
    public void test_hash_code() {
        Coordinate where = new Coordinate(1,2);
        Placement p1 = new Placement(where, 'H');
        Placement p2 = new Placement(where, 'V');
        Placement p3 = new Placement(where, 'H');

        assertEquals(p1.hashCode(), p1.hashCode());
        assertEquals(p1.hashCode(), p3.hashCode());
        assertNotEquals(p1.hashCode(), p2.hashCode());
    }

    @Test
    public void test_string_constructor_valid_cases() {
        Placement p1 = new Placement("B3H");
        assertEquals(p1.getWhere().getRow(), 1);
        assertEquals(p1.getWhere().getColumn(), 3);
        assertEquals(p1.getOrientation(), 'H');

        Placement p2 = new Placement("Z0V");
        assertEquals(p2.getWhere().getRow(), 25);
        assertEquals(p2.getWhere().getColumn(), 0);
        assertEquals(p2.getOrientation(), 'V');

        Placement p3 = new Placement("A9v");
        assertEquals(p3.getWhere().getRow(), 0);
        assertEquals(p3.getWhere().getColumn(), 9);
        assertEquals(p3.getOrientation(), 'V');
    }

    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("000"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("AA"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("@0"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("[0"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("A/"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("A:"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("A"));
        assertThrows(IllegalArgumentException.class, () -> new Coordinate("A12"));
    }
}
