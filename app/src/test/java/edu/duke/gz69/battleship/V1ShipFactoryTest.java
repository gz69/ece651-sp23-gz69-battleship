package edu.duke.gz69.battleship;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class V1ShipFactoryTest {

    public ArrayList<Coordinate> getCoordinates(Coordinate upperLeft, int width, int height){
        ArrayList<Coordinate> arrayList=new ArrayList<>();
        int startRow = upperLeft.getRow();
        int startCol = upperLeft.getColumn();
        for(int i = startRow; i < startRow+height; ++i){
            for(int j = startCol; j < startCol + width; ++j){
                arrayList.add(new Coordinate(i, j));
            }
        }
        return arrayList;
    }

    @Test
    void test_get_coordinate() {
        Ship<Character> s = new V1ShipFactory().makeDestroyer(new Placement("A0V"));
        Set<Coordinate> set = new HashSet<>();
        set.add(new Coordinate(0, 0));
        set.add(new Coordinate(1, 0));
        set.add(new Coordinate(2, 0));
        assertEquals(set, s.getCoordinates());
    }
    @Test
    void test_make_different_ships() {
        V1ShipFactory shipFactory = new V1ShipFactory();
        Coordinate c11 = new Coordinate(1, 1);

        Ship<Character> sub_v = shipFactory.makeSubmarine(new Placement(c11,'V'));
        checkShip(sub_v,"Submarine",'s',getCoordinates(c11, 1, 2));

        Ship<Character> sub_h = shipFactory.makeSubmarine(new Placement(c11,'H'));
        checkShip(sub_h,"Submarine",'s',getCoordinates(c11, 2, 1));

        Ship<Character> bat_v = shipFactory.makeBattleship(new Placement(c11,'V'));
        checkShip(bat_v,"Battleship",'b',getCoordinates(c11, 1, 4));

        Ship<Character> car_v = shipFactory.makeCarrier(new Placement(c11,'V'));
        checkShip(car_v,"Carrier",'c',getCoordinates(c11, 1, 6));

        Ship<Character> des_v = shipFactory.makeDestroyer(new Placement(c11,'V'));
        checkShip(des_v,"Destroyer",'d',getCoordinates(c11, 1, 3));

    }

    private void checkShip(Ship<Character> testShip, String expectedName,
                           char expectedLetter, ArrayList<Coordinate> expectedLocs) {
        assertEquals(testShip.getName(),expectedName);

        for(Coordinate c:expectedLocs){
            assertEquals(testShip.getDisplayInfoAt(c, true),expectedLetter);
        }
    }
}