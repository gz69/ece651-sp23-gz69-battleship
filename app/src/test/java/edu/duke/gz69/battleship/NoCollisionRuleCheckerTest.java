package edu.duke.gz69.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NoCollisionRuleCheckerTest {

    @Test
    void test_no_collision_rule() {
        PlacementRuleChecker<Character> placementChecker = new NoCollisionRuleChecker<>(new InBoundsRuleChecker<>(null));

        Placement p1 = new Placement("C6H");
        Ship<Character> s1 = new V1ShipFactory().makeBattleship(p1);
        Board<Character> b1 = new BattleShipBoard<>(10,20, placementChecker, 'X');
        assertNull(placementChecker.checkPlacement(s1, b1));

        b1.tryAddShip(s1); // this should not be alright

        Placement p2 = new Placement("C5H");
        Ship<Character> s2 = new V1ShipFactory().makeBattleship(p2);
        assertEquals("That placement is invalid: the ship overlaps another ship.", b1.tryAddShip(s2));
        assertEquals("That placement is invalid: the ship overlaps another ship.", placementChecker.checkPlacement(s2, b1));

        Placement p3 = new Placement("T8V");
        Ship<Character> s3 = new V1ShipFactory().makeBattleship(p3);
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.", placementChecker.checkPlacement(s3, b1));
    }
}