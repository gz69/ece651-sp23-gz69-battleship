package edu.duke.gz69.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InBoundsRuleCheckerTest {

    @Test
    void test_in_bound_rule() {
        PlacementRuleChecker<Character> placementCheck = new InBoundsRuleChecker<>(null);

        Placement p1 = new Placement("C6H");
        Ship<Character> s1 = new V1ShipFactory().makeBattleship(p1);
        Board<Character> b1 = new BattleShipBoard<>(10,20, 'X');
        assertNull(placementCheck.checkPlacement(s1, b1));

        Placement p2 = new Placement("C8H");
        Ship<Character> s2 = new V1ShipFactory().makeBattleship(p2);
        assertEquals("That placement is invalid: the ship goes off the right of the board.",
                placementCheck.checkPlacement(s2, b1));

        Placement p3 = new Placement("T8V");
        Ship<Character> s3 = new V1ShipFactory().makeBattleship(p3);
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.",
                placementCheck.checkPlacement(s3, b1));
    }
}