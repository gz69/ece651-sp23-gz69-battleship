package edu.duke.gz69.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;

public class BattleShipBoardTest {
    @Test
    public void test_width_and_height() {
        Board<Character> b1 = new BattleShipBoard<>(10,20, 'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
    }
    @Test
    public void test_invalid_dimensions() {
        assertThrows(IllegalArgumentException.class, () -> new
                BattleShipBoard<>(10,0, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new
                BattleShipBoard<>(0,20, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new
                BattleShipBoard<>(10,-5, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new
                BattleShipBoard<>(-8,20, 'X'));
    }

    private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected)  {
        int width = b.getWidth();
        int height = b.getHeight();

        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                T target = b.whatIsAtForSelf(new Coordinate(i,j));
                char checker = ' ';
                if(target != null) {
                    checker = (char) target;
                }
                assertEquals(checker, expected[i][j]);
            }
        }
    }



    @Test
    public void test_try_add_and_what_is_at() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(2,2, 'X');
        Character[][] expected1 = {
                {' ', ' '},
                {' ', ' '}
        };
        checkWhatIsAtBoard(b1, expected1);
        // TODO what will be done if there are two ships in the same Coordinate
        BattleShipBoard<Character> b2 = new BattleShipBoard<>(2,2, 'X');
        Placement c2 = new Placement("A0H");
        Ship<Character> s2 = new RectangleShip<>(c2, 's', '*');
        String isAdded = b2.tryAddShip(s2);
        assertNull(isAdded);
        Character[][] expected2 = {
                {'s', ' '},
                {' ', ' '}
        };
        checkWhatIsAtBoard(b2, expected2);
    }

    @Test
    public void test_sonar_area() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10,10, 'X');
        ArrayList<Coordinate> actual;
        actual = b1.getSonarArea(new Coordinate(4,4), 2);
        ArrayList<Coordinate> expected = new ArrayList<>();
        expected.add(new Coordinate(1, 1));
        assertEquals(expected, actual);
    }

    @Test
    public void test_fire_at() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(4,4, 'X');
        V1ShipFactory creator = new V1ShipFactory();
        Ship<Character> s1 = creator.makeBattleship(new Placement("A0H"));
        String isAdded = b1.tryAddShip(s1);
        System.out.println(isAdded);
        b1.fireAt(new Coordinate("H0"));
        b1.fireAt(new Coordinate("B0"));

        Ship<Character> actualShip = b1.fireAt(new Coordinate("a0"));
        assertEquals('b', b1.whatIsAtForEnemy(new Coordinate(0,0)));
        assertEquals('*', b1.whatIsAtForSelf(new Coordinate(0,0)));
        assertSame(s1, actualShip);
        assertEquals(s1, actualShip);
        HashSet<Coordinate> expectedSet = new HashSet<>();
        expectedSet.add(new Coordinate("H0"));
        expectedSet.add(new Coordinate("B0"));
        assertEquals(expectedSet, b1.enemyMisses);

        assertFalse(s1.isSunk());
        b1.fireAt(new Coordinate("a1"));
        assertFalse(s1.isSunk());
        b1.fireAt(new Coordinate("a2"));
        assertFalse(s1.isSunk());
        b1.fireAt(new Coordinate("a3"));
        assertTrue(s1.isSunk());

        assertTrue(b1.checkAllSunk());

        Ship<Character> s2 = creator.makeSubmarine(new Placement("B0V"));

        b1.tryAddShip(s2);
        assertFalse(b1.checkAllSunk());

        assertEquals(expectedSet, b1.enemyMisses);

    }
}
