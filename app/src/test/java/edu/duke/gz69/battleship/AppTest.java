package edu.duke.gz69.battleship;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.ResourceAccessMode;
import org.junit.jupiter.api.parallel.ResourceLock;
import org.junit.jupiter.api.parallel.Resources;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_main() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);

        InputStream input = getClass().getResourceAsStream("/input.txt");
        assertNotNull(input, "input.txt not found");

        InputStream expectedStream = getClass().getResourceAsStream("/output.txt");
        assertNotNull(expectedStream, "output.txt not found");

        System.setIn(input);
        System.setOut(out);
        App.main(new String[0]);
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();

        assertAll("output from App.main() method",
                () -> assertEquals(expected, actual, "output does not match"),
                () -> assertTrue(actual.contains("Hello, world!"), "output does not contain 'Hello, world!'"));
    }
}

