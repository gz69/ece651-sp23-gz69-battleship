package edu.duke.gz69.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class TShapeShipTest {

    @Test
    void test_generate_TShape_ship() {
//
//        V2ShipFactory factory = new V2ShipFactory();
//        Ship<Character> battleship = factory.makeBattleship(new Placement("A0U"));
        HashSet<Coordinate> actual = TShapeShip.makeCoords(new Placement("A0U"));
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(0,1));
        expected.add(new Coordinate(1,0));
        expected.add(new Coordinate(1,1));
        expected.add(new Coordinate(1,2));
        assertEquals(expected, actual);


        HashSet<Coordinate> actual2 = TShapeShip.makeCoords(new Placement("A0D"));
        HashSet<Coordinate> expected2 = new HashSet<>();
        expected2.add(new Coordinate(0,0));
        expected2.add(new Coordinate(0,1));
        expected2.add(new Coordinate(0,2));
        expected2.add(new Coordinate(1,1));
        assertEquals(expected2, actual2);


        HashSet<Coordinate> actual3 = TShapeShip.makeCoords(new Placement("A0R"));
        HashSet<Coordinate> expected3 = new HashSet<>();
        expected3.add(new Coordinate(0,0));
        expected3.add(new Coordinate(1,0));
        expected3.add(new Coordinate(2,0));
        expected3.add(new Coordinate(1,1));
        assertEquals(expected3, actual3);

        HashSet<Coordinate> actual4 = TShapeShip.makeCoords(new Placement("A0L"));
        HashSet<Coordinate> expected4 = new HashSet<>();
        expected4.add(new Coordinate(2,1));
        expected4.add(new Coordinate(1,1));
        expected4.add(new Coordinate(0,1));
        expected4.add(new Coordinate(1,0));
        assertEquals(expected4, actual4);
    }
}