package edu.duke.gz69.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class RectangleShipTest {

    @Test
    void test_make_coords() {
        HashSet<Coordinate> actual = RectangleShip.makeCoords(new Coordinate(1, 2), 1, 3);
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(1,2));
        expected.add(new Coordinate(2,2));
        expected.add(new Coordinate(3,2));

        assertEquals(actual, expected);
    }

    @Test
    void test_occupies() {
        SimpleShipDisplayInfo<Character> myDisplayInfo = new SimpleShipDisplayInfo<>('s', '*');
        SimpleShipDisplayInfo<Character> enemyDisplayInfo = new SimpleShipDisplayInfo<>(' ', 's');

        Ship<Character> rShip = new RectangleShip<>("submarine", new Coordinate(1, 2), 1, 3, myDisplayInfo, enemyDisplayInfo);
        assertEquals("submarine", rShip.getName());

        assertTrue(rShip.occupiesCoordinates(new Coordinate(1, 2)));
        assertTrue(rShip.occupiesCoordinates(new Coordinate(2, 2)));
        assertTrue(rShip.occupiesCoordinates(new Coordinate(3, 2)));
        assertFalse(rShip.occupiesCoordinates(new Coordinate(3,1)));

        V1ShipFactory factory = new V1ShipFactory();
        Ship<Character> ship2 = factory.makeBattleship(new Placement("A0H"));
        assertTrue(ship2.occupiesCoordinates(new Coordinate(0, 0)));
    }

    @Test
    void test_record_hit_and_was_hit_at() {
        Coordinate c11 = new Coordinate(1, 1);
        Coordinate c21 = new Coordinate(2, 1);
        Coordinate c31 = new Coordinate(3, 1);

        Ship<Character> s = new V1ShipFactory().makeDestroyer(new Placement(c11, 'V'));
        assertEquals("Destroyer", s.getName());

        s.recordHitAt(c11);
        assertTrue(s.wasHitAt(c11));
        assertEquals('d', s.getDisplayInfoAt(c21, true));
        assertEquals('*', s.getDisplayInfoAt(c11, true));

        assertNull(s.getDisplayInfoAt(c21, false));
        assertEquals('d', s.getDisplayInfoAt(c11, false));

        s.recordHitAt(c21);
        s.recordHitAt(c31);
        assertTrue(s.isSunk());

    }
}