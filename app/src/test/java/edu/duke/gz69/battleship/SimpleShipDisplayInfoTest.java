package edu.duke.gz69.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleShipDisplayInfoTest {

    @Test
    void test_get_info() {
        SimpleShipDisplayInfo<Character> info = new SimpleShipDisplayInfo<>('s', '*');

        assertEquals('*', info.getInfo(new Coordinate(1,2), true));
        assertEquals('s', info.getInfo(new Coordinate(1,2), false));
    }
}