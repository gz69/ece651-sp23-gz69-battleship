package edu.duke.gz69.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoardTextViewTest {

    @Test
    public void test_display_empty_2by2() {
        Board<Character> b1 = new BattleShipBoard<>(2, 2, 'X');
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader = "  0|1\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected =
                expectedHeader +
                        "A  |  A\n" +
                        "B  |  B\n" +
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_display_empty_3by5() {
        String expectedHeader = "  0|1|2\n";
        String expectedBody =
                "A  | |  A\n" +
                        "B  | |  B\n" +
                        "C  | |  C\n" +
                        "D  | |  D\n" +
                        "E  | |  E\n";
        emptyBoardHelper(3, 5, expectedHeader, expectedBody);
    }

    @Test
    public void test_display_empty_4by2() {
        String expectedHeader = "  0|1|2|3\n";
        String expectedBody =
                "A  | | |  A\n" +
                        "B  | | |  B\n";
        emptyBoardHelper(4, 2, expectedHeader, expectedBody);
    }

    @Test
    public void test_display_empty_4by3_with_ship() {
        Board<Character> b1 = new BattleShipBoard<>(4, 3, 'X');
        Ship<Character> s = new RectangleShip<>(new Coordinate(0, 0), 's', '*');
        b1.tryAddShip(s);
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader = "  0|1|2|3\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected =
                expectedHeader +
                        "A s| | |  A\n" +
                        "B  | | |  B\n" +
                        "C  | | |  C\n" +
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());

        b1.tryAddShip(new RectangleShip<>(new Coordinate(1,1), 's', '*'));
        String expected2 =
                expectedHeader +
                        "A s| | |  A\n" +
                        "B  |s| |  B\n" +
                        "C  | | |  C\n" +
                        expectedHeader;
        assertEquals(expected2, view.displayMyOwnBoard());

    }

    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard<>(11, 20, 'X');
        Board<Character> tallBoard = new BattleShipBoard<>(10, 27, 'X');
        assertThrows(IllegalArgumentException.class, () -> new
                BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new
                BoardTextView(tallBoard));
    }

    private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard<>(w, h, 'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    // brand-new test
    @Test
    void test_display_any_board() {
        BattleShipBoard<Character> b2 = new BattleShipBoard<>(4, 3, 'X');
        BoardTextView view = new BoardTextView(b2);
        String expectedHeader = "  0|1|2|3\n";
        assertEquals(expectedHeader, view.makeHeader());

        V1ShipFactory factory = new V1ShipFactory();
        Ship<Character> s1 = factory.makeSubmarine(new Placement("B0H"));
        Ship<Character> s2 = factory.makeDestroyer(new Placement("A3V"));

        b2.tryAddShip(s1);
        b2.tryAddShip(s2);

        String expected3 =
                expectedHeader +
                        "A  | | |d A\n" +
                        "B s|s| |d B\n" +
                        "C  | | |d C\n" +
                        expectedHeader;
        assertEquals(expected3, view.displayMyOwnBoard());

        b2.fireAt(new Coordinate("B0"));

        String expected4 =
                expectedHeader +
                        "A  | | |d A\n" +
                        "B *|s| |d B\n" +
                        "C  | | |d C\n" +
                        expectedHeader;
        assertEquals(expected4, view.displayMyOwnBoard());

        String expected5 =
                expectedHeader +
                        "A  | | |  A\n" +
                        "B s| | |  B\n" +
                        "C  | | |  C\n" +
                        expectedHeader;
        assertEquals(expected5, view.displayEnemyBoard());


        b2.fireAt(new Coordinate("C0"));
        String expected6 =
                expectedHeader +
                        "A  | | |d A\n" +
                        "B *|s| |d B\n" +
                        "C  | | |d C\n" +
                        expectedHeader;
        assertEquals(expected6, view.displayMyOwnBoard());

        String expected7 =
                expectedHeader +
                        "A  | | |  A\n" +
                        "B s| | |  B\n" +
                        "C X| | |  C\n" +
                        expectedHeader;
        assertEquals(expected7, view.displayEnemyBoard());

        assertEquals("\n" +
                        "11                       22\n" +
                        "  0|1|2|3                    0|1|2|3\n" +
                        "A  | | |d A                A  | | |  A\n" +
                        "B *|s| |d B                B s| | |  B\n" +
                        "C  | | |d C                C X| | |  C\n" +
                        "  0|1|2|3                    0|1|2|3\n",
                view.displayMyBoardWithEnemyNextToIt(view, "11", "22"));
    }
}